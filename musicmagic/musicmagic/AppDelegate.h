//
//  AppDelegate.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/15/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

