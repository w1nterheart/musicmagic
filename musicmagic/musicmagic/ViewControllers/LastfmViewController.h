//
//  LastfmViewController.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/15/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LastfmViewController : UIViewController <CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) CLLocationManager *locationManager;
@end
