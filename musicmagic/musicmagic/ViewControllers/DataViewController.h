//
//  DataViewController.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/14/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;

@end

