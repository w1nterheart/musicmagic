//
//  RootViewController.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "RootViewController.h"
#import "SoundcloudViewController.h"
#import "IPodViewController.h"
#import "SoundCloudApi.h"
#import "PlayerViewController.h"
#import "PlayerOverlayViewController.h"
#import "SettingsViewController.h"

@interface RootViewController () {
    IBOutlet UITabBarController *tabBarController;
}
@property (nonatomic, retain) UITabBarController *tabBarController;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIView *overlayPlayerView;

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setAutoresizesSubviews:true];
    
    SoundcloudViewController *SCController = [[SoundcloudViewController alloc] initWithNibName:@"SoundcloudViewController" bundle: nil];
    IPodViewController *IPodController = [[IPodViewController alloc] initWithNibName:@"IPodViewController" bundle: nil];
    SettingsViewController *SettingsController = [[SettingsViewController alloc] init];
    
    [SCController.tabBarItem setTitle: @"Soundcloud"];
    [IPodController.tabBarItem setTitle: @"iPod"];
    [SettingsController.tabBarItem setTitle:@"Settings"];
    
    self.tabBarController = [[UITabBarController alloc] init];
    [self.tabBarController setViewControllers:[NSArray arrayWithObjects:SCController, IPodController, SettingsController, nil]];
    [self addChildViewController:self.tabBarController];
    
    [self.tabBarController.view setFrame:CGRectMake(0.0f, 0.0f, self.mainContentView.frame.size.width, self.mainContentView.frame.size.height)];
    [self.mainContentView setClipsToBounds:YES];
    [self.mainContentView addSubview:self.tabBarController.view];
    
    [self addChildViewController: [PlayerOverlayViewController sharedInstance]];
    [[PlayerOverlayViewController sharedInstance].view setFrame:CGRectMake(0.0f, 0.0f, self.overlayPlayerView.frame.size.width, self.overlayPlayerView.frame.size.height)];
    [self.overlayPlayerView setClipsToBounds:YES];
    [self.overlayPlayerView addSubview:[PlayerOverlayViewController sharedInstance].view];
    
    [self.view bringSubviewToFront:self.overlayPlayerView];
}

- (void)rootTabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    switch(item.tag) {
        case 1: {
            NSLog(@"TAB 1");
            break;
        }
        case 2: {
            NSLog(@"TAB2");
            break;
        }
    }
}

//- (UITabBarController *)tabBarController {
//    if (self.tabBarController == nil) {
//        [[NSBundle mainBundle] loadNibNamed:@"TabBar" owner:self options:nil];
//    }
//    return self.tabBarController;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    self.tabBarController = nil;
}

@end
