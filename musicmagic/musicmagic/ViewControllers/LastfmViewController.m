//
//  LastfmViewController.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/15/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "LastfmViewController.h"
#import "WeatherEntryTableViewCell.h"

#define NAVIGATION_BUTTON_MARGIN ( 5.f )
#define NAVIGATION_ICON_BUTTON_SIDE ( 44.f - NAVIGATION_BUTTON_MARGIN * 2)
#define NAVIGATION_CANCEL_BUTTON_WIDTH ( 60.f )

@interface LastfmViewController () <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate> {

}
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation LastfmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

#pragma mark - UITableView Datasource & Delegate -
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 0 ? 132 : 44;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    WeatherEntryTableViewCell *cell = (WeatherEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    if (cell == nil) {
        cell = [[WeatherEntryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

#warning didSelectRowAtIndexPath
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    DetailViewController *vc = [[DetailViewController alloc] initWithWeatherEntry: _data[indexPath.row]];
//    [vc setWeatherData: _data[indexPath.row]];
//
//    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Memory -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    
}

@end
