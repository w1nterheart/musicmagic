//
//  IPodViewController.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/15/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "IPodViewController.h"
#import "PlaybackManager.h"
#import "LocalStorageManager.h"
#import "Track.h"

@interface IPodViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) NSMutableArray *songsList;
@property(nonatomic, assign, getter=isSearching) BOOL searching;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *trackListTableView;

@end

@implementation IPodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self searchTableList];
}

- (IBAction)nextButtonPressed:(id)sender {
}


- (IBAction)prevButtonPressed:(id)sender {
    
}

#pragma mark - Private methods
- (void)searchTableList {
    [[LocalStorageManager sharedInstance] getIPodSongsEntries:self.searchBar.text completion:^(NSMutableArray *responseData) {
        self.songsList = (NSMutableArray *) responseData;
        [self.trackListTableView reloadData];
        [self.view endEditing:YES];
    }];
}

#pragma mark - Search Bar
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

#pragma mark - TableView

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Track *pickedTrack = self.songsList[indexPath.row];
    [[PlaybackManager sharedInstance] setData: pickedTrack];
//    [[PlaybackManager sharedInstance] setSoundcloudData: self.songsList[indexPath.row]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.songsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    Track *songDictionary = [self.songsList objectAtIndex:indexPath.row];
    [cell.textLabel setText:songDictionary.title];
    [cell.detailTextLabel setText:songDictionary.artist];
    
    return cell;
}

#pragma mark - Helpers
- (NSAttributedString *) createAttributtedStringFromHTMLText:(NSString *)htmlText {
    NSError *error;
    NSAttributedString *attributtedString = [[NSAttributedString alloc] initWithData: [htmlText dataUsingEncoding:NSUTF8StringEncoding]
                                                                             options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                  documentAttributes: nil
                                                                               error: &error];
    if(error) {
        NSLog(@"Unable to parse label text: %@", error);
    }

    return attributtedString;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
