//
//  SoundcloudViewController.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/15/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "SoundcloudViewController.h"
#import "SoundCloudApi.h"
#import "MBProgressHUD.h"
#import "LastfmViewController.h"
#import "ConnectionManager.h"
#import "PlaybackManager.h"
#import "Track.h"

@interface SoundcloudViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) NSMutableArray *songsList;
@property(nonatomic, assign, getter=isSearching) BOOL searching;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *trackListTableView;

@property (nonatomic, retain) MBProgressHUD *hud ;

@end

@implementation SoundcloudViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self searchTableList];
}

- (IBAction)nextButtonPressed:(id)sender {
}


- (IBAction)prevButtonPressed:(id)sender {
    
}

#pragma mark - Private methods
- (void)searchTableList {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.label.text = @"Loading";
    [[ConnectionManager sharedInstance] getSongsEntries: [self.searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]
                                               completion:^(NSMutableArray *requestData) {
                                                   self.songsList = (NSMutableArray *) requestData;
                                                   [self.trackListTableView reloadData];
                                                   [self.view endEditing:YES];
                                                   [self.hud hideAnimated:YES];
                                               }];
}

#pragma mark - Search Bar
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
    [self.searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

#pragma mark - TableView

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Track *pickedTrack = self.songsList[indexPath.row];
    if (pickedTrack.data == nil) {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.label.text = @"Loading";
        self.hud.detailsLabel.text = [NSString stringWithFormat:@"%@ - %@", pickedTrack.title, pickedTrack.artist];
        [[SoundCloudApi sharedInstance] getItemAtUrl: pickedTrack.URL completion:^(NSData *soundcloudEntry) {
            [pickedTrack setData:soundcloudEntry];
            self.songsList[indexPath.row] = pickedTrack;
            [[PlaybackManager sharedInstance] setData:pickedTrack];
            [self.hud hideAnimated:YES];
        }];
    } else {
        [[PlaybackManager sharedInstance] setData:pickedTrack];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.songsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    Track *song = [self.songsList objectAtIndex:indexPath.row];
    
    [cell.textLabel setAttributedText:[self createAttributtedStringFromHTMLText:song.title]];
    [cell.detailTextLabel setText:song.artist];
    
    return cell;
}

#pragma mark - Helpers
- (NSAttributedString *) createAttributtedStringFromHTMLText:(NSString *)htmlText {
    NSError *error;
    NSAttributedString *attributtedString = [[NSAttributedString alloc] initWithData: [htmlText dataUsingEncoding:NSUTF8StringEncoding]
                                                                             options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                  documentAttributes: nil
                                                                               error: &error];
    if(error) {
        NSLog(@"Unable to parse label text: %@", error);
    }

    return attributtedString;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
