//
//  RootViewController.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController<UIApplicationDelegate, UITabBarDelegate>

@end
