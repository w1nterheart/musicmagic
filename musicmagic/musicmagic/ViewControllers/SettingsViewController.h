//
//  SettingsViewController.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 9/1/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InAppSettingsKit/IASKAppSettingsViewController.h"

@interface SettingsViewController : IASKAppSettingsViewController

@end
