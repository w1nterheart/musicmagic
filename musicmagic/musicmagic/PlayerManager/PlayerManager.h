//
//  DCHSoundCloudPlayer.h
//  Pods
//
//  Created by Daniel Chirita on 3/10/16.
//
//

#import <Foundation/Foundation.h>
#import "DCHSoundCloudApiProgressHandler.h"
#import "Track.h"

@import AVFoundation;

#define BLOCK_EXEC(block, ...) if (block) { block(__VA_ARGS__); };

@interface PlayerManager : NSObject<AVAudioPlayerDelegate>{
    BOOL _isPlaying;
}

@property (nonatomic, assign, readonly) BOOL isPlaying;
@property (nonatomic, strong) Track *track;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, copy)   DCHSoundCloudApiProgressHandler progressHandler;
@property (nonatomic, strong) NSTimer *playbackTimer;

+ (PlayerManager *) sharedInstance;

- (void)setTrack:(Track *) track;

- (void)playWithProgressHandler:(DCHSoundCloudApiProgressHandler)handler;

- (void) play;

- (void) stop;

- (void) pause;

@end
