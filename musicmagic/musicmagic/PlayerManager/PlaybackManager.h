//
//  PlaybackManager.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Track.h"

@interface PlaybackManager : NSObject {
    Track *_track;
    
    NSTimeInterval *_maxTime;
    NSTimeInterval *_currentTime;
}

@property (nonatomic, retain) Track *track;

//@property (nonatomic, retain) NSTimeInterval *maxTime;
//@property (nonatomic, retain) NSTimeInterval *currentTime;

+ (PlaybackManager *) sharedInstance;

- (void) setData: (Track *) song;
- (void) togglePlayback;

@end
