//
//  PlaybackManager.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "PlaybackManager.h"
#import "SoundCloudApi.h"
#import "PlayerOverlayViewController.h"
#import "PlayerViewController.h"
#import "PlayerManager.h"
#import "Track.h"

@implementation PlaybackManager

+ (PlaybackManager *) sharedInstance {
    static PlaybackManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PlaybackManager alloc] init];
    });
    return instance;
}

- (void) setData: (Track*) song {
    _track = song;
    if ([[PlayerManager sharedInstance] isPlaying]) {
        [[PlayerManager sharedInstance] stop];
    }
    
    [self play];
}

- (void) togglePlayback {
    if (![[PlayerManager sharedInstance] isPlaying]) {
        [[PlayerManager sharedInstance] play];
    } else {
        [[PlayerManager sharedInstance] pause];
    }
    
    [[PlayerViewController sharedInstance] updateCurrentValues];
    [[PlayerOverlayViewController sharedInstance] updateCurrentValues];
}

- (void) play {
    if (_track == nil) {
        NSLog(@"Track is nil");
        return;
    }
    
    [[PlayerManager sharedInstance] setTrack:_track];
    [[PlayerManager sharedInstance] playWithProgressHandler:^(NSTimeInterval duration, NSTimeInterval currentTime, NSError *error) {
        _currentTime = &currentTime;
        _maxTime = &duration;
        
        [[PlayerViewController sharedInstance] updateCurrentTimeValues: &currentTime maxTime: &duration];
    }];
    
    [[PlayerViewController sharedInstance] updateCurrentValues];
    [[PlayerOverlayViewController sharedInstance] updateCurrentValues];
}

@end
