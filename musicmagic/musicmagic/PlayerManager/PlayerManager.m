//
//  DCHSoundCloudPlayer.m
//  Pods
//
//  Created by Daniel Chirita on 3/10/16.
//
//  Rewritten by Vitalii Iroshnikov 8/20/16.
//

#import "PlayerManager.h"

@implementation PlayerManager

#pragma mark - Public

+ (PlayerManager *) sharedInstance {
    static PlayerManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PlayerManager alloc] init];
    });
    return instance;
}

- (void)setTrack:(Track *) track {
    _track = track;
}

- (void) playWithProgressHandler:(DCHSoundCloudApiProgressHandler) handler{
    self.progressHandler = handler;
    
    NSError *error;
    self.audioPlayer = _track.data == nil ?
        [[AVAudioPlayer alloc] initWithContentsOfURL:_track.URL error:&error] :
        [[AVAudioPlayer alloc] initWithData:_track.data error:&error];
    
    [self checkErrorAndPlay:error];
}

- (void) checkErrorAndPlay: (NSError *) error {
    self.audioPlayer.delegate = self;
    
    if (!error){
        _isPlaying = YES;
        [self.audioPlayer play];
    }else{
        [self updateListenerWithError:error];
    }
    
    [self updateListener];
    [self startUpdatingListener];
}

- (void) play {
    _isPlaying = YES;
    
    [self.audioPlayer play];
}


- (void) stop {
    _isPlaying = NO;
    
    [self.audioPlayer stop];
    
    BLOCK_EXEC(self.progressHandler, self.audioPlayer.duration, 0, nil);
    [self cleanUp];
}

- (void) pause {
    _isPlaying = NO;
    
    [self.audioPlayer pause];
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    if (flag) {
        [self updateListener];
        [self cleanUp];
    }
}

- (void) audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    [self updateListenerWithError:error];
    [self cleanUp];
}

#pragma mark - Private

- (void) startUpdatingListener {
    self.playbackTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(updateListener)
                                                        userInfo:nil
                                                         repeats:YES];
}

- (void) updateListener {
    BLOCK_EXEC(self.progressHandler, self.audioPlayer.duration, self.audioPlayer.currentTime, nil);
}

- (void) updateListenerWithError:(NSError *)error {
    BLOCK_EXEC(self.progressHandler, 0.f, 0.f, error);
}

- (void) cleanUp {
    [self.playbackTimer invalidate];
    self.playbackTimer = nil;
    self.audioPlayer = nil;
}

@end
