//
//  WeatherEntryTodayTableViewCell.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/21/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "WeatherEntryTodayTableViewCell.h"

@implementation WeatherEntryTodayTableViewCell

@synthesize dateTextField = _dateTextField;
@synthesize forecastTextField = _forecastTextField;
@synthesize maxTempTextField = _maxTempTextField;
@synthesize minTempTextField = _minTempTextField;
@synthesize forecastIcon = _forecastIcon;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if (self) {
        CGSize size = self.contentView.frame.size;
        
        self.backgroundColor = [UIColor colorWithRed:40./255. green:185./255. blue:244./255. alpha:1.];
        
        self.dateTextField = [[UILabel alloc] initWithFrame: CGRectMake(40., 0, size.width - 24., size.height - 16.)];
        [self.dateTextField setFont: [UIFont boldSystemFontOfSize: 14.f]];
        [self.dateTextField setTextAlignment:NSTextAlignmentLeft];
        [self.dateTextField setTextColor:[UIColor whiteColor]];
        [self addSubview: self.dateTextField];
        
        self.forecastTextField = [[UILabel alloc] initWithFrame: CGRectMake(size.width - 96., 100., 48., size.height - 16.)];
        [self.forecastTextField setFont: [UIFont fontWithName:@"HelveticaNeue" size: 14.f]];
        [self.forecastTextField setTextAlignment:NSTextAlignmentLeft];
        [self.forecastTextField setTextColor:[UIColor whiteColor]];
        [self addSubview: self.forecastTextField];
        
        self.maxTempTextField = [[UILabel alloc] initWithFrame: CGRectMake(40., 36., 64., size.height - 16.)];
        [self.maxTempTextField setFont:[UIFont boldSystemFontOfSize:32.f]];
        [self.maxTempTextField setTextAlignment:NSTextAlignmentLeft];
        [self.maxTempTextField setTextColor:[UIColor whiteColor]];
        [self addSubview: self.maxTempTextField];
        
        self.minTempTextField = [[UILabel alloc] initWithFrame: CGRectMake(40., 64, 48., size.height - 16.)];
        [self.minTempTextField setFont:[UIFont fontWithName:@"HelveticaNeue" size: 16.f]];
        [self.minTempTextField setTextAlignment:NSTextAlignmentLeft];
        [self.minTempTextField setTextColor:[UIColor whiteColor]];
        [self addSubview: self.minTempTextField];
        
        self.forecastIcon = [[UIImageView alloc] initWithFrame: CGRectMake(size.width - 128., 0., 96., 96.)];
        [self addSubview: self.forecastIcon];
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints: YES];
    }
    
    return self;
}

- (void) updateConstraints {
    [super updateConstraints];
    
    
//    NSDictionary *views = NSDictionaryOfVariableBindings(self.maxTempTextField, _minTempTextField);
}

@end
