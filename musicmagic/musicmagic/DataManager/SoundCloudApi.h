//
//  SoundCloudApi.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/17/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCHSoundCloudApiProgressHandler.h"

@interface SoundCloudApi : NSObject

+ (instancetype) sharedInstance;

+ (void)setClientID:(NSString *)clientID;

- (void) getItemAtUrl: (NSURL *) url completion: (void (^)(NSData *)) completion;

@end

