//
//  LocalStorageManager.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/28/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalStorageManager : NSObject {
    @private
    dispatch_queue_t enumerationQueue;
}

+ (LocalStorageManager *) sharedInstance;

- (void) getIPodSongsEntries: (NSString *) query completion: (void (^)(NSMutableArray *)) completion;

@end
