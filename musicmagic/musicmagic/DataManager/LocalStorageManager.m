//
//  LocalStorageManager.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/28/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "LocalStorageManager.h"

#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "Track.h"

@implementation LocalStorageManager

+ (LocalStorageManager *) sharedInstance {
    static LocalStorageManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[LocalStorageManager alloc] init];
        instance->enumerationQueue = dispatch_queue_create("Browser Enumeration Queue", DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(instance->enumerationQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
    });
    
    return instance;
}

- (void) getIPodSongsEntries: (NSString *) query completion: (void (^)(NSMutableArray *)) completion {
    dispatch_async(enumerationQueue, ^(void) {
        NSMutableArray *items = [NSMutableArray arrayWithCapacity:0];
        for (MPMediaItem *mediaItem in [[[MPMediaQuery alloc] init] items]) {
            if ((NSURL*)[mediaItem valueForProperty:MPMediaItemPropertyAssetURL]) {
                Track *item = [[Track alloc] initWithURL:(NSURL*)[mediaItem valueForProperty:MPMediaItemPropertyAssetURL] title:(NSString*)[mediaItem valueForProperty:MPMediaItemPropertyTitle]];
                [item setArtist: (NSString*)[mediaItem valueForProperty:MPMediaItemPropertyArtist]];
                [item setAlbum: (NSString*)[mediaItem valueForProperty:MPMediaItemPropertyAlbumTitle]];
                [items addObject:item];
            }
        }
       
        completion(items);
    });
}

@end
