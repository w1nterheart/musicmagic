//
//  Utility.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/19/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (Utility *) sharedInstance;
- (NSString *) getIconResourceForWeatherCondition: (int) weatherID;

@end
