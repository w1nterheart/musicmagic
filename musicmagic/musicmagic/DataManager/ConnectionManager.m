//
//  ConnectionManager.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/16/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "ConnectionManager.h"
#import "Track.h"
//#import "NetworkActivity.h"

@implementation ConnectionManager

+ (ConnectionManager *) sharedInstance {
    static ConnectionManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ConnectionManager alloc] init];
    });
    return instance;
}

- (void) getSongsEntries: (NSString *) query completion: (void (^)(NSMutableArray *)) completion {
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/?client_id=%@&q=%@&format=json", URI_FETCH_TRACKS, SOUNDCLOUD_API_KEY, query]];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (data == nil) {
                                                            return;
                                                        }
                                                        
                                                        if (error) {
                                                            NSLog(@"ERROR: %@", [error localizedDescription]);
                                                        }
                                                        
                                                        NSError *err = nil;
                                                        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error:&err];
                                                        
                                                        if (err) {
                                                            NSLog(@"ERROR: %@", [err localizedDescription]);
                                                        }
                                                    
                                                        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
                                                        NSInteger statusCode = [httpResponse statusCode];
                                                        
                                                        NSMutableArray *items = [NSMutableArray arrayWithCapacity:0];
                                                        for (NSDictionary *soundcloudEntry in dict) {
                                                            NSURL *URL = (NSURL*) soundcloudEntry[@"permalink_url"];
                                                            if (URL) {
                                                                Track *item = [[Track alloc] initWithURL:URL title:(NSString*)soundcloudEntry[@"title"]];
                                                                [item setArtist: soundcloudEntry[@"user"][@"username"]];
                                                                [item setAlbum: soundcloudEntry[@""] ? soundcloudEntry[@""] :  @"Soundcloud"];
                                                                [items addObject:item];
                                                            }
                                                        }
                                                        
                                                        completion(items);
                                                    }];
    
    [dataTask resume];
}

@end
