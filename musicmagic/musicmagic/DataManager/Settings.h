//
//  Settings.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject

+(Settings*) sharedInstance;

- (BOOL) wasFirstStart;
- (void) setWasFirstStart: (BOOL) value;

- (BOOL) isAuthenticated;
- (void) setAuthenticated: (BOOL) value;

@end
