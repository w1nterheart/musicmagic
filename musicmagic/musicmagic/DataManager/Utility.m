//
//  Utility.m
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/19/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+ (Utility *) sharedInstance {
    static Utility* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Utility alloc] init];
    });
    return instance;
}

- (NSString *) getIconResourceForWeatherCondition: (int) weatherID {
    
    NSString *image = @"";
    if (weatherID >= 200 && weatherID <= 232) {
        image = @"ic_storm";
    } else if (weatherID >= 300 && weatherID <= 321) {
        image = @"ic_light_rain";
    } else if (weatherID >= 500 && weatherID <= 504) {
        image = @"ic_rain";
    } else if (weatherID == 511) {
        image = @"ic_snow";
    } else if (weatherID >= 520 && weatherID <= 531) {
        image = @"ic_rain";
    } else if (weatherID >= 600 && weatherID <= 622) {
        image = @"ic_snow";
    } else if (weatherID >= 701 && weatherID <= 761) {
        image = @"ic_fog";
    } else if (weatherID == 761 || weatherID == 781) {
        image = @"ic_storm";
    } else if (weatherID == 800) {
        image = @"ic_clear";
    } else if (weatherID == 801) {
        image = @"ic_light_clouds";
    } else if (weatherID >= 802 && weatherID <= 804) {
        image = @"ic_cloudy";
    }
    
    if ([image isEqualToString: @""]) {
        image = @"ic_snow";
    }
    
    return image;
}


@end
