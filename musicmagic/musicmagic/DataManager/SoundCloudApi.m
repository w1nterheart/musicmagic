//
//  SoundCloudApi.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/17/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "SoundCloudApi.h"
#import "DCHSoundCloudResolve.h"

#import "Bolts.h"
#import "CocoaLumberjack.h"

static SoundCloudApi *sharedInstance = nil;

@interface SoundCloudApi()
@property (nonatomic, copy) NSString *clientID;
@end

@implementation SoundCloudApi

#pragma mark - Public

+ (instancetype) sharedInstance {
    NSAssert(nil != sharedInstance && sharedInstance.clientID != nil, @"[SoundCloudApi setClientID:] has to be called before using sharedInstance");
    
    return sharedInstance;
}

+ (void)setClientID:(NSString *) clientID {
    sharedInstance = [[SoundCloudApi alloc] init];
    sharedInstance.clientID = clientID;
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
}

- (void) getItemAtUrl: (NSURL *) url completion: (void (^)(NSData *)) completion {
    NSAssert(0 != ((NSString *) url).length, @"SoundCloudApi empty url was passed");
    [[DCHSoundCloudResolve startAsyncWithUrl: (NSString *) url clientId:self.clientID] continueWithBlock:^id(BFTask *task){
        NSURL *trackURL = [NSURL URLWithString:task.result];
        NSURLSessionTask *sessionTask =
        [[NSURLSession sharedSession] dataTaskWithURL:trackURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (error) {
                    NSLog(@"ERROR GETTING DATA: %@", [error localizedDescription]);
                    return;
                }
                
                completion(data);
            });
        }];
        
        [sessionTask resume];
        return nil;
    }];
}

@end

