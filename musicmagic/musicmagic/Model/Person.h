//
//  Person.h
//  sunshine
//
//  Created by Vitaliy Lopatkin on 3/15/15.
//  Copyright (c) 2015 ExiriInc. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

@interface Person : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * isFriend;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSData * photoData;
@property (nonatomic, retain) NSString * personID;

@end
