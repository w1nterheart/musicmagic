//
//  Track.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <CoreMedia/CMTime.h>

@class AVAsset;

enum {
    AssetBrowserItemFillModeCrop,
    AssetBrowserItemFillModeAspectFit
};

typedef NSInteger AssetBrowserItemFillMode;
@interface Track : NSObject <NSCopying> {
@private
    NSURL *assetURL;
    
    UIImage *thumbnailImage;
    NSString *assetTitle;
    BOOL haveRichestTitle;
    
    AVAsset *asset;
    
    BOOL audioOnly;
    BOOL canGenerateThumbnails;
    
    dispatch_queue_t assetQueue;
}

- (id)initWithURL:(NSURL*)URL;
- (id)initWithURL:(NSURL*)URL title:(NSString*)title; // title can be nil.
- (void) setArtist:(NSString*) artistTitle;
- (void) setAlbum:(NSString*) albumTitle;
- (void) setData:(NSData *)newData;

/* With AssetBrowserItemFillModeAspectFit size acts as a maximum size. Pass CGRectZero for a full size thumbnail;
 With AssetBrowserItemFillModeCrop the image is cropped to fit size. If the asset does not have enough resolution
 than the returned image have be the aspect ratio specified by size, but lower resolution.
 Retrieve the generated thumbnail with the thumbnailImage property. */
- (void)generateThumbnailAsynchronouslyWithSize:(CGSize)size fillMode:(AssetBrowserItemFillMode)mode completionHandler:(void (^)(UIImage *thumbnail))handler;
- (UIImage*)placeHolderImage;

- (void)generateTitleFromMetadataAsynchronouslyWithCompletionHandler:(void (^)(NSString *title))handler;

@property (nonatomic, readonly) NSURL *URL;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *album;
@property (nonatomic, readonly, copy) NSString *artist;
@property (nonatomic, readonly, copy) NSData *data;
@property (nonatomic, readonly) BOOL haveRichestTitle;
@property (nonatomic, readonly, strong) UIImage *thumbnailImage;
@property (weak, nonatomic, readonly) AVAsset *asset;

- (void)clearThumbnailCache;
- (void)clearAssetCache;

@end
