//
//  Track.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "Track.h"

#import <AVFoundation/AVFoundation.h>

#include <AssertMacros.h>


@interface Track ()

- (AVAsset*)copyAssetIfCreated;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *artist;
@property (nonatomic, copy) NSString *album;
@property (nonatomic, copy) NSData *data;
@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, readonly) BOOL canGenerateThumbnails;
@property (nonatomic, readonly) BOOL audioOnly;

@end


@implementation Track

@synthesize URL = assetURL, haveRichestTitle;
@synthesize thumbnailImage, canGenerateThumbnails, audioOnly;

- (id)initWithURL:(NSURL*)URL
{
    return [self initWithURL:URL title:nil];
}

- (id)initWithURL:(NSURL*)URL title:(NSString*)title
{
    if ((self = [super init])) {
        assetURL = URL;
        if (assetURL == nil) {
            return nil;
        }
        haveRichestTitle = title ? YES : NO;
        assetTitle = title ? [title copy] : [[URL lastPathComponent] stringByDeletingPathExtension];
        
        // Assume we can generate a thumb unless we have loaded the assets or tried already and know otherwise.
        canGenerateThumbnails = YES;
        
        // Assets can only be accessed from one thread at a time.
        assetQueue = dispatch_queue_create("Asset Queue", DISPATCH_QUEUE_SERIAL);
        _title = assetTitle;
        _artist = nil;
        _album = nil;
        _data = nil;
    }
    return self;
}

- (void) setArtist:(NSString *)artistTitle {
    _artist = artistTitle;
}

- (void) setAlbum:(NSString *)albumTitle {
    _album = albumTitle;
}

- (void) setData:(NSData *)newData {
    _data = newData;
}

- (id)initWithAssetItem:(Track*)browserItem
{
    if ((self = [super init])) {
        // Inititialization time properties.
        assetURL = browserItem.URL;
        
        // May have been an initialization time property.
        assetTitle = browserItem.title;
        haveRichestTitle = browserItem.haveRichestTitle;
        
        thumbnailImage = browserItem.thumbnailImage;
        asset = [browserItem copyAssetIfCreated];
        
        canGenerateThumbnails = browserItem.canGenerateThumbnails;
        audioOnly = browserItem.audioOnly;
        
        assetQueue = dispatch_queue_create("Asset Queue", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return [[Track allocWithZone:zone] initWithAssetItem:self];
}

// Do simple equality based on the item's URL.
- (BOOL)isEqual:(id)anObject
{
    if (self == anObject) {
        return YES;
    }
    
    if ([anObject isKindOfClass:[Track class]]) {
        Track *item = anObject;
        NSURL *myURL = self.URL;
        NSURL *theirURL = item.URL;
        if (myURL && theirURL) {
            return [myURL isEqual:theirURL];
        }
    }
    
    return NO;
}

- (NSUInteger)hash
{
    return self.URL ? [self.URL hash] : [super hash];
}


@end
