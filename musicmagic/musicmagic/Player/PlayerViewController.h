//
//  PlayerViewController.h
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/19/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerViewController : UIViewController {
@private
    float mRestoreAfterScrubbingRate;
    BOOL seekToZeroBeforePlay;
    id mTimeObserver;
    BOOL isSeeking;
}

+ (PlayerViewController *) sharedInstance;

- (void) updateCurrentValues;
- (void) updateCurrentTimeValues: (NSTimeInterval *) currentTime maxTime: (NSTimeInterval *) maxTime;

@end
