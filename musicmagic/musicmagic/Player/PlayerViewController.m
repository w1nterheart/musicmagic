//
//  PlayerViewController.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/19/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "PlayerViewController.h"
#import "PlaybackManager.h"
#import "PlayerManager.h"

@interface PlayerViewController ()

@property (weak, nonatomic) IBOutlet UILabel *minTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxTimeLabel;

@property (weak, nonatomic) IBOutlet UILabel *currentTrackNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentArtistNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *prevTrackButton;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *nextTrackButton;

@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;

@property (weak, nonatomic) IBOutlet UIButton *hideCurrentControllerButton;
@end

@implementation PlayerViewController

+ (PlayerViewController *) sharedInstance {
    static PlayerViewController* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PlayerViewController alloc] initWithNibName: @"PlayerOverlayView" bundle:nil];
    });
    return instance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateCurrentValues];
}

- (IBAction)hideCurrentControllerButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)playPauseButtonPressed:(id)sender {
    [[PlaybackManager sharedInstance] togglePlayback];
}

- (void) updateCurrentTimeValues: (NSTimeInterval *) currentTime maxTime: (NSTimeInterval *) maxTime {
    self.currentTimeSlider.value = (*currentTime * 100)/ *maxTime;
}

- (void) updateCurrentValues {
    _currentTrackNameLabel.text = [[PlaybackManager sharedInstance] track].title;
    _currentArtistNameLabel.text = [NSString stringWithFormat:@"%@, %@", [[PlaybackManager sharedInstance] track].artist, [[PlaybackManager sharedInstance] track].album];
    
    [self.playPauseButton setImage:[UIImage imageNamed:[[PlayerManager sharedInstance] isPlaying] ? @"pause-64px.png" : @"play-64px.png"] forState: UIControlStateNormal];
}


/* The user is dragging the movie controller thumb to scrub through the movie. */
- (IBAction)beginScrubbing:(id)sender
{
    mRestoreAfterScrubbingRate = [[PlayerManager sharedInstance].audioPlayer rate];
    [[PlayerManager sharedInstance].audioPlayer setRate:0.f];
    
    /* Remove previous timer. */
    [self removePlayerTimeObserver];
}

/* Set the player current time to match the scrubber position. */
- (IBAction)scrub:(id)sender
{
    if ([sender isKindOfClass:[UISlider class]] && !isSeeking) {
        isSeeking = YES;
        UISlider* slider = sender;
        
        if (![[PlayerManager sharedInstance] isPlaying]) {
            return;
        }
        
        NSTimeInterval time = [PlayerManager sharedInstance].audioPlayer.duration * ([slider value] - [slider minimumValue]) / ([slider maximumValue] - [slider minimumValue]);
        [[PlayerManager sharedInstance].audioPlayer setCurrentTime:time];
        isSeeking = NO;
    }
}


/* The user has released the movie thumb control to stop scrubbing through the movie. */
- (IBAction)endScrubbing:(id)sender
{
    if (!mTimeObserver)
    {
        NSTimeInterval playerDuration = [self playerItemDuration];
        if (isfinite(playerDuration))
        {
            CGFloat width = CGRectGetWidth([_currentTimeSlider bounds]);
            double tolerance = 0.5f * playerDuration / width;
            
            __weak PlayerViewController *weakSelf = self;
//            mTimeObserver = [[PlayerManager sharedInstance].audioPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC) queue:NULL usingBlock:
//                             ^(CMTime time)
//                             {
//                                 [weakSelf syncScrubber];
//                             }];
        }
    }
    
    if (mRestoreAfterScrubbingRate) {
        [[PlayerManager sharedInstance].audioPlayer setRate:mRestoreAfterScrubbingRate];
        mRestoreAfterScrubbingRate = 0.f;
    }
}


/* ---------------------------------------------------------
 **  Get the duration for a AVPlayerItem.
 ** ------------------------------------------------------- */

- (NSTimeInterval)playerItemDuration
{
    return([[PlayerManager sharedInstance] isPlaying] ? [PlayerManager sharedInstance].audioPlayer.duration : 0);
}


/* Cancels the previously registered time observer. */
-(void)removePlayerTimeObserver
{
    if (mTimeObserver) {
//        [[PlayerManager sharedInstance].audioPlayer removeTimeObserver:mTimeObserver];
        mTimeObserver = nil;
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
