//
//  PlayerOverlayViewController.m
//  musicmagic
//
//  Created by Vitalii Iroshnikov on 8/20/16.
//  Copyright © 2016 Vitaliy Lopatkin. All rights reserved.
//

#import "PlayerOverlayViewController.h"
#import "PlaybackManager.h"
#import "PlayerManager.h"

@interface PlayerOverlayViewController () {
    UITapGestureRecognizer *_gestureRecognizer;
}

@property (weak, nonatomic) IBOutlet UILabel *trackNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistAlbumNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *togglePlaybackButton;

@end

@implementation PlayerOverlayViewController

+ (PlayerOverlayViewController *) sharedInstance {
    static PlayerOverlayViewController* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PlayerOverlayViewController alloc] initWithNibName: @"PlayerOverlayViewController" bundle:nil];
    });
    return instance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(revealPlayerViewLabelPressed:)];
    
    [self.trackNameLabel setUserInteractionEnabled:YES];
    [self.trackNameLabel addGestureRecognizer:_gestureRecognizer];
    [self.artistAlbumNameLabel setUserInteractionEnabled:YES];
    [self.artistAlbumNameLabel addGestureRecognizer:_gestureRecognizer];
}

- (void) updateCurrentValues {
    self.trackNameLabel.text = [[PlaybackManager sharedInstance] track].title;
    self.artistAlbumNameLabel.text = [NSString stringWithFormat:@"%@, %@", [[PlaybackManager sharedInstance] track].artist, [[PlaybackManager sharedInstance] track].album];
    
    [self.togglePlaybackButton setBackgroundImage:[UIImage imageNamed:[[PlayerManager sharedInstance] isPlaying] ? @"pause-64px.png" : @"play-64px.png"]  forState: UIControlStateNormal];
}

- (IBAction)togglePlaybackButtonPressed:(id)sender {
    [[PlaybackManager sharedInstance] togglePlayback];
}

- (IBAction)revealPlayerViewLabelPressed:(id)sender {
    [self presentViewController:[PlayerViewController sharedInstance] animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
